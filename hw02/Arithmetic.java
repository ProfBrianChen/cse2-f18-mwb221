/* Max Bonzulak, 9-13-18, CSE 002 
   This program computes the cost of retail items, including tax*/
public class Arithmetic //this class computes the cost of retail items, including tax
{
  public static void main (String [] args) //This method computes the cost of retail items, including tax
  {
    int numPants = 3;//Number of pairs of pants
    double pantsPrice = 34.98;//Cost per pair of pants
    int numShirts = 2;//Number of sweatshirts
    double shirtPrice = 24.99;//Cost per shirt
    int numBelts = 1;//Number of belts
    double beltCost = 33.99;//cost per belt
    double paSalesTax = 0.06; //the tax rate
    double totalCostOfPants = numPants * pantsPrice;   //total cost of pants
    double totalCostOfShirts = numShirts * shirtPrice;   //total cost of sweatshirts
    double totalCostOfBelts = numBelts * beltCost;   //total cost of belts
    double taxOnPants = totalCostOfPants * paSalesTax; //tax on pants
    double taxOnShirts = totalCostOfShirts * paSalesTax; //tax on sweatshirts
    double taxOnBelts = totalCostOfBelts * paSalesTax; //tax on belts
    double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //cost before taxOnBelts
    double totalTax = taxOnPants + taxOnShirts + taxOnBelts; //total tax
    double totalIncTax = totalCost + totalTax; //total including taxOnPants
    System.out.println("The total cost of Pants is $"+((int)(100*totalCostOfPants))/100.0+". The tax on these items is $"+((int)(100*taxOnPants))/100.0+".");
    System.out.println("The total cost of Sweatshirts is $"+((int)(100*totalCostOfShirts))/100.0+". The tax on these items is $"+((int)(100*taxOnShirts))/100.0+".");
    System.out.println("The total cost of Belts is $"+((int)(100*totalCostOfBelts))/100.0+". The tax on these items is $"+((int)(100*taxOnBelts))/100.0+".");
    System.out.println("The total cost items is $"+((int)(100*totalCost))/100.0+" plus $"+((int)(100*totalTax))/100.0+" tax.");
    System.out.println("Therefore, your grand total is $"+((int)(100*totalIncTax))/100.0);
  }
}