import java.util.Random;
import java.util.Scanner;
public class Lab07
{
  public static String adjective()
  {
    Random rand = new Random();
    int num = rand.nextInt(9);
    String output = "";
    switch (num)
    {
      case 0: 
        output = "round";
        break;
      case 1: 
        output = "green";
        break;
      case 2: 
        output = "smelly";
        break;
      case 3: 
        output = "graceful";
        break;
      case 4: 
        output = "soft";
        break;
      case 5: 
        output = "crisp";
        break;
      case 6: 
        output = "sour";
        break;
      case 7: 
        output = "large";
        break;
      case 8: 
        output = "intelligent";
        break;
    }
    return output;
  }
  public static String subject()
  {
    Random rand = new Random();
    int num = rand.nextInt(9);
    String output = "";
    switch (num)
    {
      case 0: 
        output = "leopard";
        break;
      case 1: 
        output = "squirrel";
        break;
      case 2: 
        output = "astronaut";
        break;
      case 3: 
        output = "athlete";
        break;
      case 4: 
        output = "man";
        break;
      case 5: 
        output = "woman";
        break;
      case 6: 
        output = "salesperson";
        break;
      case 7: 
        output = "commentator";
        break;
      case 8: 
        output = "person";
        break;
    }
    return output;
  }
  public static String verb()
  {
    Random rand = new Random();
    int num = rand.nextInt(9);
    String output = "";
    switch (num)
    {
      case 0: 
        output = "ate";
        break;
      case 1: 
        output = "destroyed";
        break;
      case 2: 
        output = "chased";
        break;
      case 3: 
        output = "loved";
        break;
      case 4: 
        output = "attacked";
        break;
      case 5: 
        output = "helped";
        break;
      case 6: 
        output = "escaped";
        break;
      case 7: 
        output = "found";
        break;
      case 8: 
        output = "received";
        break;
    }
    return output;
  }
  public static String directObject()
  {
    Random rand = new Random();
    int num = rand.nextInt(9);
    String output = "";
    switch (num)
    {
      case 0: 
        output = "rock";
        break;
      case 1: 
        output = "tree";
        break;
      case 2: 
        output = "gift";
        break;
      case 3: 
        output = "apple";
        break;
      case 4: 
        output = "telephone";
        break;
      case 5: 
        output = "soup";
        break;
      case 6: 
        output = "sink";
        break;
      case 7: 
        output = "bed";
        break;
      case 8: 
        output = "frog";
        break;
    }
    return output;
  }
  public static String thesis (String adjective1, String subject, String verb, String adjective2, String directObject)
  {
    return ("The "+adjective1+" "+subject+" "+verb+" the "+adjective2+" "+directObject+". ");
  }
  public static String action (String subject)
  {
    Random rand = new Random();
    int num = rand.nextInt(2);
    if (num == 0)
    {
      subject = "It";
    }
    else 
    {
      subject = "The "+subject;
      System.out.println(num);
    }
    return (subject+" "+verb()+" the "+adjective()+" "+subject()+". ");
  }
  public static String conclusion (String subject)
  {
    return ("That "+subject+" "+verb()+" his "+directObject()+"s!");
  }
  public static String paragraph (String adjective1, String subject, String verb, String adjective2, String directObject)
  {
    Random rand = new Random();
    String output = "";
    output+= thesis(adjective1, subject, verb, adjective2, directObject);
    int num = 0;
    while (num<=3)
    {
      num = rand.nextInt(6);
    }
    int c = 1;
    while (c <= num)
    {
      output+= action(subject);
      if (c%2 == 0)
      {
        output+= "\n";
      }
      c++;
    }
    output+= conclusion(subject);
    return output;
  }
  public static void main (String [] args)
  {
    Scanner scan = new Scanner(System.in);
    String adjective1 = "";
    String subject = "";
    String verb = "";
    String adjective2 = "";
    String directObject = "";
    int input = -1;
    boolean validInput = false;
    boolean newSentence = true;
    while (newSentence)
    {
      adjective1 = adjective();
      subject = subject();
      verb = verb();
      adjective2 = adjective();
      directObject = directObject();
      
      System.out.println("The "+adjective1+" "+subject+" "+verb+" the "+adjective2+" "+directObject+". ");
      System.out.println("Would you like a different sentence? Type 0 for no, 1 for yes");
      input = scan.nextInt();
      if (input==0)
      {
        validInput = true;
        newSentence = false;
      }
      if (input==1)
      {
        validInput = true;
        newSentence = true;
      }
      while (!validInput)
      {
        System.out.println("Input invalid, please enter 0 or 1");
        input = scan.nextInt();
        if (input==0)
        {
          validInput = true;
          newSentence = false;
        }
        if (input==1)
        {
          validInput = true;
          newSentence = true;
        }
      }
    }
    System.out.println("\n"+paragraph(adjective1, subject, verb, adjective2, directObject));
  }
}