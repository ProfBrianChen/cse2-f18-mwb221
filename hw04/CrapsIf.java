
import java.util.Scanner; //imports scanner class
public class CrapsIf //creates the class
{
  public static void main (String args []) //creates the main method
  {
    Scanner myScanner = new Scanner( System.in ); //declares an instance and calls the scanner constructor
    
    System.out.println("\n\nWould you like to input your own data or generate random numbers?"); //prompts user to input desired number source
    System.out.println("Enter \"1\" to input your own data or\nEnter \"2\" to generate random.");
    
    int numberSource = 0; //initializes numberSource which represents the source of the dice's numbers.
    boolean validInput = false; //initializes validInput

    while (validInput == false) //records user's desired source of numbers 'till the input is valid
    {
      numberSource = myScanner.nextInt(); //records user's desired source of numbers
      if (numberSource == 1 || numberSource == 2) //if input fits requirements, sets validInput to true
      {
        validInput = true; //sets validInput to true
      }
      else //if input did not fit requirements, prints error message and loop restarts 
      {
        System.out.println("Input not valid, please try again."); //error message
      }
    }
    
    int num1 = 0; //initializes num1 which will represent the first die
    int num2 = 0; //initializes num2 which will represent the second die
    
    if (numberSource == 1) //records num1 and num2 from user if user requested to self-input
    {
      System.out.println("Please enter the number for the first die."); //prompts user to input number for die #1
      validInput = false; //resets validInput
      while (validInput == false) //records user's first desired number 'till the input is valid
      {
        num1 = myScanner.nextInt(); //records user's first desired number
        if (num1 >= 1 && num1 <= 6) //if input fits requirements, sets validInput to true
        {
          validInput = true; //sets validInput to true
        }
        else //if input did not fit requirements, prints error message and loop restarts 
        {
          System.out.println("Input not valid, please enter an integer from 1 to 6."); //error message
        }
      }
      System.out.println("Please enter the number for the second die."); //prompts user to input number for die #1
      validInput = false; //resets validInput
      while (validInput == false) //records user's second desired number 'till the input is valid
      {
        num2 = myScanner.nextInt(); //records user's second desired number
        if (num2 >= 1 && num2 <= 6) //if input fits requirements, sets validInput to true
        {
          validInput = true; //sets validInput to true
        }
        else //if input did not fit requirements, prints error message and loop restarts 
        {
          System.out.println("Input not valid, please enter an integer from 1 to 6."); //error message
        }
      }
    }
    if (numberSource == 2) //generates random numbers for num1 and num2 if user requested random
    {
      while (num1 < 1 || num1 > 6) //generates a number for die #one 'till it falls within range
      {
        num1 = (int)(10 * Math.random()); //generates a random integer from 0 to 9
      }
      while (num2 < 1 || num2 > 6) //generates a number for die #two 'till it falls within range
      {
        num2 = (int)(10 * Math.random()); //generates a random integer from 0 to 9
      }
    }
    
    String name = ""; //Initializes name which will ultimately be printed
    
    if (num1 == 1 || num2 == 1) //goes through all combinations where at least one die equals 1
    {
      if (num1 == 1 && num2 == 1) //checks if both dice are 1s
      {
        name = "Snake Eyes"; //sets name to Snake Eyes
      }
      if (num1 == 2 || num2 == 2) //checks if either die is a 2
      {
        name = "Ace Deuce"; //sets name to Ace Deuce
      }
      if (num1 == 3 || num2 == 3) //*** Process continues for every combination ****
      {
        name = "Easy Four";
      }
      if (num1 == 4 || num2 == 4)
      {
        name = "Fever Five";
      }
      if (num1 == 5 || num2 == 5)
      {
        name = "Easy Six";
      }
      if (num1 == 6 || num2 == 6)
      {
        name = "Seven out";
      }
    }
    if (num1 == 2 || num2 == 2) //goes through all remaining combinations where at least one die equals 2
    {
      if (num1 == 2 && num2 == 2) 
      {
        name = "Hard four";
      }
      if (num1 == 3 || num2 == 3)
      {
        name = "Fever five";
      }
      if (num1 == 4 || num2 == 4)
      {
        name = "Easy six";
      }
      if (num1 == 5 || num2 == 5)
      {
        name = "Seven out";
      }
      if (num1 == 6 || num2 == 6)
      {
        name = "Easy Eight";
      }
    }
    if (num1 == 3 || num2 == 3) //goes through all remaining combinations where at least one die equals 3
    {
      if (num1 == 3 && num2 == 3) 
      {
        name = "Hard six";
      }
      if (num1 == 4 || num2 == 4)
      {
        name = "Seven out";
      }
      if (num1 == 5 || num2 == 5)
      {
        name = "Easy Eight";
      }
      if (num1 == 6 || num2 == 6)
      {
        name = "Nine";
      }
    }
    if (num1 == 4 || num2 == 4) //goes through all remaining combinations where at least one die equals 4
    {
      if (num1 == 4 && num2 == 4) 
      {
        name = "Hard Eight";
      }
      if (num1 == 5 || num2 == 5)
      {
        name = "Nine";
      }
      if (num1 == 6 || num2 == 6)
      {
        name = "Easy Ten";
      }
    }
    if (num1 == 5 || num2 == 5) //goes through all remaining combinations where at least one die equals 5
    {
      if (num1 == 5 && num2 == 5) 
      {
        name = "Hard Ten";
      }
      if (num1 == 6 || num2 == 6)
      {
        name = "Yo-leven";
      }
    }
    if (num1 == 6 && num2 == 6) //goes through all remaining combinations where at least one die equals 6
    {
      name = "Boxcars";
    }
    System.out.println(name); //prints the slang name for the combination
  }
}