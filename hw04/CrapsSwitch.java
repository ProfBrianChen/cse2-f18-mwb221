
import java.util.Scanner; //imports scanner class
public class CrapsSwitch //creates the class
{
  public static void main (String args []) //creates the main method
  {
    Scanner myScanner = new Scanner( System.in ); //declares an instance and calls the scanner constructor
    
    System.out.println("\n\nWould you like to input your own data or generate random numbers?"); //prompts user to input desired number source
    System.out.println("Enter \"1\" to input your own data or\nEnter \"2\" to generate random.");
    
    int numberSource = 0; //initializes numberSource which represents the source of the dice's numbers.
    boolean validInput = false; //initializes validInput

    while (validInput == false) //records user's desired source of numbers 'till the input is valid
    {
      numberSource = myScanner.nextInt(); //records user's desired source of numbers
      if (numberSource == 1 || numberSource == 2) //if input fits requirements, sets validInput to true
      {
        validInput = true; //sets validInput to true
      }
      else //if input did not fit requirements, prints error message and loop restarts 
      {
        System.out.println("Input not valid, please try again."); //error message
      }
    }
    
    int num1 = 0; //initializes num1 which will represent the first die
    int num2 = 0; //initializes num2 which will represent the second die
    
    if (numberSource == 1) //records num1 and num2 from user if user requested to self-input
    {
      System.out.println("Please enter the number for the first die."); //prompts user to input number for die #1
      validInput = false; //resets validInput
      while (validInput == false) //records user's first desired number 'till the input is valid
      {
        num1 = myScanner.nextInt(); //records user's first desired number
        if (num1 >= 1 && num1 <= 6) //if input fits requirements, sets validInput to true
        {
          validInput = true; //sets validInput to true
        }
        else //if input did not fit requirements, prints error message and loop restarts 
        {
          System.out.println("Input not valid, please enter an integer from 1 to 6."); //error message
        }
      }
      System.out.println("Please enter the number for the second die."); //prompts user to input number for die #1
      validInput = false; //resets validInput
      while (validInput == false) //records user's second desired number 'till the input is valid
      {
        num2 = myScanner.nextInt(); //records user's second desired number
        if (num2 >= 1 && num2 <= 6) //if input fits requirements, sets validInput to true
        {
          validInput = true; //sets validInput to true
        }
        else //if input did not fit requirements, prints error message and loop restarts 
        {
          System.out.println("Input not valid, please enter an integer from 1 to 6."); //error message
        }
      }
    }
    if (numberSource == 2) //generates random numbers for num1 and num2 if user requested random
    {
      while (num1 < 1 || num1 > 6) //generates a number for die #one 'till it falls within range
      {
        num1 = (int)(10 * Math.random()); //generates a random integer from 0 to 9
      }
      while (num2 < 1 || num2 > 6) //generates a number for die #two 'till it falls within range
      {
        num2 = (int)(10 * Math.random()); //generates a random integer from 0 to 9
      }
    }
    
    String name = ""; //Initializes name which will ultimately be printed
    
    switch (num1) //enters a switch statement for the first die
    {
      case 1: switch (num2) //goes through each combination where the first die is 1
      {
        case 1: name = "Snake Eyes"; //changes the name to Snake Eyes if the second die is 1
        break; //breaks from case 1 for the nested switch statement
        case 2: name = "Ace Deuce"; //etc...
        break;
        case 3: name = "Easy Four";
        break;
        case 4: name = "Fever Five";
        break;
        case 5: name = "Easy Six";
        break;
        case 6: name = "Seven Out";
        break;
      }
      break; //breaks from case 1 for the main switch statement
      case 2: switch (num2) //goes through each combination where the first die is 2
      {
        case 1: name = "Ace Deuce"; //same progression as in previous nested switch statement
        break; 
        case 2: name = "Hard Four";
        break;
        case 3: name = "Fever Five";
        break;
        case 4: name = "Easy Six";
        break;
        case 5: name = "Seven Out";
        break;
        case 6: name = "Easy Eight";
        break;
      }
      break;
      case 3: switch (num2) //goes through each combination where the first die is 3
      {
        case 1: name = "Easy Four";
        break;
        case 2: name = "Fever Five";
        break;
        case 3: name = "Hard Six";
        break;
        case 4: name = "Seven Out";
        break;
        case 5: name = "Easy Eight";
        break;
        case 6: name = "Nine";
        break;
      }
      break;
      case 4: switch (num2) //goes through each combination where the first die is 4
      {
        case 1: name = "Fever Five";
        break;
        case 2: name = "Easy Six";
        break;
        case 3: name = "Seven Out";
        break;
        case 4: name = "Hard Eight";
        break;
        case 5: name = "Nine";
        break;
        case 6: name = "Easy Ten";
        break;
      }
      break;
      case 5: switch (num2) //goes through each combination where the first die is 5
      {
        case 1: name = "Easy Six";
        break;
        case 2: name = "Seven Out";
        break;
        case 3: name = "Easy Eight";
        break;
        case 4: name = "Nine";
        break;
        case 5: name = "Hard Ten";
        break;
        case 6: name = "Yo-leven";
        break;
      }
      break;
      case 6: switch (num2) //goes through each combination where the first die is 6
      {
        case 1: name = "Seven Out";
        break;
        case 2: name = "Easy Eight";
        break;
        case 3: name = "Nine";
        break;
        case 4: name = "Easy Ten";
        break;
        case 5: name = "Yo-leven";
        break;
        case 6: name = "Boxcars";
        break;
      }
      break;
    }
    System.out.println(name); //prints the slang name for the combination
  }
}