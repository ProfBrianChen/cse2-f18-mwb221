/* Max Bonzulak, 10-20-18, CSE 002 
   This program prints a number pyramid*/

import java.util.Scanner;
public class PatternA
{
  public static void main (String [] args)
  {
    Scanner scan = new Scanner (System.in); //declares an instance and calls the scanner constructor
    System.out.println ("Enter an integer from 1-10."); //promts user to give input
    int num = 0;
    int s = 0;
    boolean validInput = false;
    while (!validInput)
    {
      
      if (scan.hasNextInt())
      {
        s = scan.nextInt();
        if (s>0 && s<11)
        {
          validInput = true;
          num = s;
        }
        else
        {
          System.out.println("Wrong input. Please enter an integer from 1- 10.");
        }
      }
      else
      {
        System.out.println("Wrong input. Please enter an integer from 1- 10.");
        scan.next();
      }
    }
    
   
    int x = 1;
    int y = 1;
    while (x<=num)
    {
      while (y<=x)
      {
        System.out.print(y+" ");
        y++;
      }
      y=1;
      System.out.println();
      x++;
    }
  }
}