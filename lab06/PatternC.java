import java.util.Scanner;
public class PatternC
{
  public static void main (String [] args)
  {
    Scanner scan = new Scanner (System.in); //declares an instance and calls the scanner constructor
    System.out.println ("Enter an integer from 1-10."); //promts user to give input
    int num = 0;
    int s = 0;
    boolean validInput = false;
    while (!validInput)
    {
      
      if (scan.hasNextInt())
      {
        s = scan.nextInt();
        if (s>0 && s<11)
        {
          validInput = true;
          num = s;
        }
        else
        {
          System.out.println("Wrong input. Please enter an integer from 1- 10.");
        }
      }
      else
      {
        System.out.println("Wrong input. Please enter an integer from 1- 10.");
        scan.next();
      }
    }
    
    
    
    int x = 1;
    int y = 1;
    int z = 1;
    while (x<=num)
    {
      y=x;
      while (z<=(num-x))
        {
          System.out.print(" ");
          z++;
        }
        z=1;
      while (y>=1)
      {
        System.out.print(y);
        y--;
      }
      System.out.println();
      x++;
    }
  }
}