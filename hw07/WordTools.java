/* Max Bonzulak, 9-13-18, CSE 002 
   This program provides various operations for a sample text */
import java.util.Scanner;
public class WordTools
{
  public static String sampleText()
  {
    System.out.println("Enter a sample text:"); //requests text
    Scanner scan = new Scanner(System.in); //creates object for scanner
    String sT = scan.nextLine(); //sample text
    System.out.println("\nYou entered:"+sT); //prints response
    return sT; //returns text
  }
  public static char printMenu()
  {
    System.out.println("Menu\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit\n\nChoose an option:"); //displays menu
    Scanner scan = new Scanner(System.in); //creates object for scanner
    return scan.next().charAt(0); //returns input
  }
  public static int getNumOfNonWSCharacters(String str)
  {
    int i = 0; //incrementor
    int counter = 0; //counts WS characters
    int l = str.length(); //stores string length
    while (i <= (l-1)) //till i exceeds the index of the last element,
    {
      if (str.charAt(i) == ' ') //if the element at i equals ' ',
      {
        counter++; //increment counter
      }
      i++; //increment i
    }
    return l-counter; //returns the difference between characters and WS characters, aka the NON WS characters
  }
  public static int getNumOfWords(String str)
  {
    int i = 0; //incrementor
    int counter = 0; //counts words
    int l = str.length(); //stores length
    while (i <= (l-1)) //till i exceeds the index of the last element,
    {
      if (str.charAt(i) != ' ') //if the element at i equals ' ',
      {
        counter++; //increment counter
        while (i<=l-1 && str.charAt(i) != ' ') //till i equals ' ' OR i exceeds the index of the last element
        {
          i++; //increment i
        }
      }
      else
      {
        i++; //increment i
      }
    }
    return counter; //returns counter
  }
  public static int findText(String phrase, String str)
  {
    int sL = str.length(); //stores str length
    int pL = phrase.length(); //stores phrase length
    int counter = 0; //counts instances of phrase in str
    int i = 0; //incrementor to move through str
    int j = 0; //incrementor to move through the phrase
    int k = 0; //temporary incrementor to compare str to phrase
    boolean validMatch = false; //declares validMatch
    while (i <= sL-pL) // till i equals the index of the last possible location 'phrase' could begin
    {
      validMatch = true; //sets valid match to true
      j = 0; //sets j to the index of the first element of 'phrase'
      k = i; //sets k to the current index of 'str'
      while (j <= pL-1) //till j exceeds the index of phrase's last element
      {
        if (phrase.charAt(j) != str.charAt(k)) //unless the element of phrase at j equals the element of str at k,
        {
          validMatch = false; //set validMatch to false
        }
        j++; //increment j
        k++; //increment k
      }
      if (validMatch) //if validMatch hasnt been set to false,
      {
        counter++; //increment counter
      }
      i++; //increment i
    }
    return counter; //return counter
  }
  public static String replaceExclamation(String str)
  {
    int i = 0; //declare i, to be an incrementor
    int l = str.length(); //store string length
    String str2 = ""; //declare str2, to store the modified string
    int marker = -1; //declare marker, the index of the most recent exclamation point
    while (i <= (l-1)) //till i exceeds the index of the last element,
    {
      if (str.charAt(i) == '!') //if the element at i equals '!'
      {
        str2 += str.substring(marker+1, i)+"."; //add [the text starting at the index after marker and ending at the index before i] and a period to str2
        marker = i; //set marker to i
      }
      i++; //increment i
    }
    if (str.charAt(i-1) != '!') //unless the last element in str is an exclamation point,
    {
      str2 += str.substring(marker+1, i); //add [the text starting at the index after marker and ending at the index of the last element] to str 2
    }
    return str2; //return str2
  }
  public static String shortenSpace(String str)
  {
    int i = 1; //declare i, to increment
    int l = str.length(); //store length of string 
    String str2 = ""; //declare str2, to store modified string 
    int marker = -1; //declare marker, the index of the most first space in the most recent chain of spaces
    while (i <= (l-1)) //till i exceeds the index of the last element of str,
    {
      if (str.charAt(i) == ' ' && str.charAt(i-1) == ' ') //if the character at i and the next character both equal ' ',
      {
        str2 += str.substring(marker+1, i); //add [the text starting at the index after marker and ending at the index before i] to str2
        marker = i; //set marker to i
      }
      i++; //increment i
    }
    if (str.charAt(i-1) != ' ' || str.charAt(i-2) != ' ') //unless the last two elements in str are ' ',
    {
      str2 += str.substring(marker+1, i); //add [the text starting at the index after marker and ending at the index of the last element] to str 2
    }
    return str2; //return str2
  }
  public static void main (String [] args)
  {
    Scanner scan = new Scanner(System.in);
    String sT = sampleText(); //sample text
    boolean qR = false; //quit requested
    char mR = ' '; //menu response
    String phrase = ""; //declare phrase to store the phrase
    while (!qR) //unless the user quits,
    {
      mR = printMenu(); //print menu and return user response
      if (mR == 'q')
      {
        qR = true; //indicate that quit has been requested
      }
      if (mR == 'c')
      {
        System.out.println("Number of non-whitespace characters: "+getNumOfNonWSCharacters(sT)); //print response and call method
      }
      if (mR == 'w')
      {
        System.out.println("Number of words: "+getNumOfWords(sT)); //print response and call method
      }
      if (mR == 'f')
      {
        System.out.println("Enter a word or phrase to be found:"); //request phrase
        phrase = scan.nextLine(); //record phrase
        System.out.println("\""+phrase+"\" instances: "+findText(phrase, sT)); //print response and call method
      }
      if (mR == 'r')
      {
        System.out.println(replaceExclamation(sT)); //print response and call method
      }
      if (mR == 's')
      {
        System.out.println(shortenSpace(sT)); //print response and call method
      }
    }
  }
}