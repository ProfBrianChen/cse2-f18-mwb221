/* Max Bonzulak, 9-13-18, CSE 002 
   This program generates a random playing card*/

import java.Math; //imports the math class
public class CardGenerator //creates the class
{
  public static void main (String [] args) //main method
  {
    int num = 100; //initializes 'num' which will hold the random number
    
    while (num < 1 || num > 52) //this loop assigns a different random integer (from 0 to 99) to num untill that integer lies between 1 and 52 (inclusive) 
    {
      num = (int)(100 * Math.random()); //generates a random integer from 0-99 
    }
    String suit = ""; //initializes suit
    String name = ""; //initializes name
    
    //this block assigns suits based on the interval the number falls between
    if (num <= 13) //checks if num is less than or equal to 13
    {
      suit = "Diamonds"; //assigns the proper name
    }
    else if (num <= 26) //the same pattern follows for the rest of the block
    {
      suit = "Hearts";
    }
    else if (num <= 39)
    {
      suit = "Clubs";
    }
    else if (num <= 52)
    {
      suit = "Spades";
    }
    
    //this block assigns numbers and names based on the remainder once divided by 13.
    if (num%13 == 1) //checks if the remainder is equal to one
    {
      name = "Ace"; //assigns the proper name
    }
    if (num%13 == 2) //the same pattern follows for the rest of the block
    {
      name = "2";
    }
    if (num%13 == 3)
    {
      name = "3";
    }
    if (num%13 == 4)
    {
      name = "4";
    }
    if (num%13 == 5)
    {
      name = "5";
    }
    if (num%13 == 6)
    {
      name = "6";
    }
    if (num%13 == 7)
    {
      name = "7";
    }
    if (num%13 == 8)
    {
      name = "8";
    }
    if (num%13 == 9)
    {
      name = "9";
    }
    if (num%13 == 10)
    {
      name = "10";
    }
    if (num%13 == 11)
    {
      name = "Jack";
    }
    if (num%13 == 12)
    {
      name = "Queen";
    }
    if (num%13 == 0)
    {
      name = "King";
    }
    
    System.out.println("You picked the "+name+" of "+suit+"."); //prints the results to the screen
  }
}