/*     Max Bonzulak, 11-26-18, CSE 002 
   This program stores an array of grades. */
import java.util.Scanner; 
public class RemoveElements 
{
  public static void main(String [] arg)
  {
	  Scanner scan=new Scanner(System.in); 
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
  	String answer="";
	  do
    {
  	  System.out.print("Random input 10 ints [0-9]");
  	  num = randomInput();
  	  String out = "The original array is:";
  	  out += listArray(num);
  	  System.out.println(out);
 
  	  System.out.print("Enter the index ");
  	  index = scan.nextInt();
    	newArray1 = delete(num,index);
  	  String out1="The output array is ";
  	  out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
    	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	  target = scan.nextInt();
    	newArray2 = remove(num,target);
  	  String out2="The output array is ";
  	  out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	  System.out.println(out2);
  	 
    	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	  answer=scan.next();
	  }
    while(answer.equals("Y") || answer.equals("y"));
  }

  public static String listArray(int num[])
  {
	  String out="{";
	  for(int j=0;j<num.length;j++)
    {
  	  if(j>0)
      {
    	  out+=", ";
  	  }
  	  out+=num[j];
	  }
	  out+="} ";
	  return out;
  }
  public static int[] randomInput()
  {
    int[] arr=new int[10];
    int i = 0;
    while (i<=10-1)
    {
      arr[i] = (int)(Math.random()*(9));
      i++;
    }
    return arr;
  }
  public static int[] delete(int[] num, int index)
  {
    int[] arr=new int[9];
    int i = 0;
    if (index>=0 && index<=10)
    {
      while (i<index)
      {
        arr[i] = num[i];
        i++;
      }
      while (i<=9-1)
      {
        arr[i] = num[i+1];
        i++;
      }
    }
    else
    {
      System.out.println("Error. Index out of range.");
    }
    return arr;
  }
  public static int[] remove(int[] num, int target)
  {
    int i = 0;
    int c = 0;
    while (i <= num.length-1)
    {
      if (num[i]==target)
      {
        c++;
      }
      i++;
    }
    int [] arr = new int [10-c];
    i = 0;
    int j = 0;
    while (i < num.length-1)
    {
      if (num[i]!=target)
      {
        arr[j]=num[i];
        j++;
      }
      i++;
    }
    return arr;
  }
}