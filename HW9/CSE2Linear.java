/*            Max Bonzulak, 11-26-18, CSE 002 
   This program stores and manipulates an array of grades. */

import java.util.Scanner; 
import java.util.Random; 
public class CSE2Linear 
{
  public static void main (String [] args)
  {
    Scanner scan = new Scanner (System.in); //declares an instance and calls the scanner constructor
    
    int[] grades = new int[15]; 
    System.out.println("\n\nEnter 15 ascending ints (one at a time) for final grades in CSE2:\n");
    int i = 0;
    int num = 0;
    boolean validInput = false;
    System.out.print("Grade #"+(i+1)+": ");
    while (!validInput)
    {
      if (scan.hasNextInt())
      {
        num = scan.nextInt();
        if (num>(-1) && num<101)
        {
          grades[i] = num;
          validInput = true;
        }
        else
        {
          System.out.print("Input is not in range. Try again: ");
        }
      }
      else
      {
        System.out.print("Input is not an integer. Try again: ");
        scan.next();
      }
    }
    i++;
    while (i <= grades.length-1)
    {
      validInput = false;
      System.out.print("Grade #"+(i+1)+": ");
      while (!validInput)
      {
        if (scan.hasNextInt())
        {
          num = scan.nextInt();
          if (num>(-1) && num<101)
          {
            if (num >= grades[i-1])
            {
              grades[i] = num;
              validInput = true;
            }
            else
            {
              System.out.print("Input is less than previous. Try again: ");
            }
          }
          else
          {
            System.out.print("Input is not in range. Try again: ");
          }
        }
        else
        {
          System.out.print("Input is not an integer. Try again: ");
          scan.next();
        }
      }
      i++;
    }
    System.out.print("\n\nYour input is: ");
    i = 0;
    while (i <= 13)
    {
      System.out.print(grades[i]+", ");
      i++;
    }
    System.out.println(grades[i]);
    System.out.print("\nEnter a grade to be searched for: ");
    int grade = scan.nextInt();
    binarySearch(grades, grade);
    grades = scramble(grades);
    System.out.print("\nNow scrambled, the current array is: ");
    i = 0;
    while (i <= 13)
    {
      System.out.print(grades[i]+", ");
      i++;
    }
    System.out.println(grades[i]);
    System.out.print("\nEnter a grade to be searched for: ");
    grade = scan.nextInt();
    linearSearch (grades, grade);
    System.out.println("\n ");
  }
  public static void binarySearch (int[] grades, int grade)
  {
    int i = (grades.length-1)/2;
    boolean searchComplete = false;
    int max = 15;
    int min = -1;
    int iterations = 0;
    String found = "not ";
    while (!searchComplete)
    {
      if (grade == grades[i])
      {
        found = "";
        searchComplete = true;
        iterations++;
      }
      if (grade > grades[i])
      {
        min = i;
        i = i+((max-min)/2);
        iterations++;
      }
      if (grade < grades[i])
      {
        max = i;
        i = i-((max-min)/2);
        iterations++;
      }
      if (max-min==1)
      {
        searchComplete = true;
      }
    }
    System.out.println(grade+" was "+found+"found in the list with "+iterations+" iterations");
  }
  public static int[] scramble(int[] grades)
  {
    int i = 0;
    int index = 0;
    int temp0 = 0;
    while (i <= 200)
    {
      index = (int) (Math.random()*(15-1));
      temp0 = grades[0];
      grades[0] = grades[index];
      grades[index] = temp0;
      i++;
    }
    return grades;
  }
  public static void linearSearch (int[] grades, int grade)
  {
    int i = 0;
    boolean searchComplete = false;
    int iterations = 0;
    String found = "not ";
    while (i <= 14 && !searchComplete)
    {
      if (grade == grades[i])
      {
        found = "";
        searchComplete = true;
      }
      i++;
      iterations++;
    }
    System.out.println(grade+" was "+found+"found in the list with "+iterations+" iterations");
  }
}