/* Max Bonzulak, 9-12-18, CSE 002 
   This program takes the time and wheel rotations for bike trips and calculates distance.*/
public class Cyclometer //this class takes the time and wheel rotations for bike trips and calculates distance.
{
  public static void main (String [] args) //This method takes the time and wheel rotations for bike trips and calculates distance.
  {
    //input data
    int secsTrip1=480;  //duration of trip one in seconds
    int secsTrip2=3220;  //duration of trip two in seconds
		int countsTrip1=1561;  //number of wheel roations for trip one 
		int countsTrip2=9037; //number of wheel rotations for trip two
    
    //intermediate variables and output data
    double wheelDiameter=27.0,  //wheel diameter in inches
  	PI=3.14159, //Pi to 6 digits
  	feetPerMile=5280,  //number of feet in one mile
  	inchesPerFoot=12,   //number of inches in one foot
  	secondsPerMinute=60;  //number of seconds in one minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //declaration of distance variables
    
    //prints the initial information
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts."); //prints info for trip one
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts."); //prints info for trip two
    
    //runs calculations for distances and stores the values
	  distanceTrip1=countsTrip1*wheelDiameter*PI; //distance in inches for trip one
  	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles for trip one
  	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives distance in miles for trip two
	  totalDistance=distanceTrip1+distanceTrip2; //total distance in miles for both trips
    
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints calculated distance for trip one
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints calculated distance for trip two
  	System.out.println("The total distance was "+totalDistance+" miles"); //prints calculated total distance 
  }
}