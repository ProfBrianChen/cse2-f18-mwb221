import java.util.Scanner; //imports scanner class
public class Course //creates class
{
  public static void main (String args []) //main method
  {
    
    
    //Records course number
    Scanner scan = new Scanner (System.in); //declares an instance and calls the scanner constructor
    System.out.println ("Think of a course you're taking.\nEnter the course number for this course..."); //promts user to give input
    while (!scan.hasNextInt()) //records input and checks if input is not valid
    {
      System.out.println("Please enter an integer"); //error message
      scan.next(); //clears the "conveyor belt"
    }
    scan.next(); //clears the "conveyor belt"
    
    
    //records department name
    System.out.println ("Enter the department name for this course..."); //promts user to give input
    while (!scan.hasNext()) //records input and checks if input is not valid
    {
      System.out.println("Please enter a string"); //error message
      scan.next(); //clears the "conveyor belt"
    }
    scan.next(); //clears the "conveyor belt"
    
    
    //Records number of times course meets per week
    System.out.println ("How many times does this course meet in a week?"); //promts user to give input
    while (!scan.hasNextInt()) //records input and checks if input is not valid
    {
      System.out.println("Please enter an integer"); //error message
      scan.next(); //clears the "conveyor belt"
    }
    scan.next(); //clears the "conveyor belt"
    
    
    //Records time course starts
    System.out.println ("What time does the course start? Enter in the form of XX.XX"); //promts user to give input
    while (!scan.hasNextDouble()) //records input and checks if input is not valid
    {
      System.out.println("Please enter an Double"); //error message
      scan.next(); //clears the "conveyor belt"
    }
    scan.next(); //clears the "conveyor belt"
    
    
    //Records instructor name
    System.out.println ("Enter the instructor's name..."); //promts user to give input
    while (!scan.hasNext()) //records input and checks if input is not valid
    {
      System.out.println("Please enter a String"); //error message
      scan.next(); //clears the "conveyor belt"
    }
    scan.next(); //clears the "conveyor belt"
    
    
    //Records number of students
    System.out.println ("Enter the number of students in this course..."); //promts user to give input
    while (!scan.hasNextInt()) //records input and checks if input is not valid
    {
      System.out.println("Please enter an integer"); //error message
      scan.next(); //clears the "conveyor belt"
    }
    scan.next(); //clears the "conveyor belt"
    
  }
}