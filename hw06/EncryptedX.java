/* Max Bonzulak, 10-22-18, CSE 002 
   This program generates an encrypted X */

import java.util.Scanner; //imports Scanner class
public class EncryptedX //creates the class
{
  public static void main (String [] args)
  {
    Scanner scan = new Scanner (System.in); //declares an instance and calls the scanner constructor
    System.out.println ("Enter an integer between 0-100 (inclusive)."); //promts user to give input
    int num = 0;
    boolean validInput = false;
    while (!validInput)
    {
      if (scan.hasNextInt())
      {
        num = scan.nextInt();
        if (num>(-1) && num<101)
        {
          validInput = true;
        }
        else
        {
          System.out.println("Wrong input. Please enter an integer from 1- 100.");
        }
      }
      else
      {
        System.out.println("Wrong input. Please enter an integer from 1- 100.");
        scan.next();
      }
    }
    int row = 1;
    int c = 1;
    
    
    while (row <= (num/2))
    {
      c = 1;
      while (c <= row-1)
      {
        System.out.print("X");
        c++;
      }
      System.out.print(" ");
      c = 1;
      while (c <= (num-row*2))
      {
        System.out.print("X");
        c++;
      }
      System.out.print(" ");
      c = 1;
      while (c <= row-1)
      {
        System.out.print("X");
        c++;
      }
      System.out.println();
      row++;
    }
    row--;
    if ((num%2)==1)
    {
      c = 1;
      while (c <= num/2)
      {
        System.out.print("X");
        c++;
      }
      System.out.print(" ");
      c = 1;
      while (c <= num/2)
      {
        System.out.print("X");
        c++;
      }
      System.out.println();
    }
    while (row >= 1)
    {
      c = 1;
      while (c <= row-1)
      {
        System.out.print("X");
        c++;
      }
      System.out.print(" ");
      c = 1;
      while (c <= (num-row*2))
      {
        System.out.print("X");
        c++;
      }
      System.out.print(" ");
      c = 1;
      while (c <= row-1)
      {
        System.out.print("X");
        c++;
      }
      System.out.println();
      row--;
    }
  }
}