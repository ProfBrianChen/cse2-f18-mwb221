/* Max Bonzulak, 9-13-18, CSE 002 
   This program generates a random playing card*/

import java.util.Scanner; //imports Scanner class
public class Hw05 //creates the class
{
  public static void main (String [] args) //main method
  {
    Scanner scan = new Scanner (System.in);
    System.out.println("How many hands?");
    
    boolean validInput = false;
    while (!validInput)
    {
      if (scan.hasNextInt())
      {
        validInput = true;
      }
      else
      {
        System.out.println("Please enter an integer.");
        scan.next();
      }
    }
    
    int hands = scan.nextInt();
    
    int num1 = 0; //initializes 'num' which will hold the random number
    int num2 = 0;
    int num3 = 0;
    int num4 = 0;
    int num5 = 0;
    
    int c1 = 0; // initializes c which will hold the card numbers
    int c2 = 0;
    int c3 = 0;
    int c4 = 0;
    int c5 = 0;
    
    int pair = 0; //creates counter for one pair
    int twoPair = 0;
    int three = 0;
    int threeDetected = 0; //stores the number on the three cards if three of a kind is found
    int four = 0;
    int fourDetected = 0;
    
    int counter = 1;
   
    while (counter <= hands)
    {
      threeDetected = 0;//resets detection values
      fourDetected = 0;
      
      num1 = 0; //resets num values
      num2 = 0;
      num3 = 0;
      num4 = 0;
      num5 = 0;
      
      c1 = 0; // resets card values
      c2 = 0;
      c3 = 0;
      c4 = 0;
      c5 = 0;
      
      while (num1 < 1 || num1 > 52) //this loop assigns a different random integer (from 0 to 99) to num untill that integer lies between 1 and 52 (inclusive) 
      {
        num1 = (int)(100 * Math.random()); //generates a random integer from 0-99
        c1 = num1 % 13; //determines num's equivilant card
        if (c1 == 0)
        {
          c1 = 13;
        }
      }
      while (num2 < 1 || num2 > 52 || num1 == num2) //this loop assigns a different random integer (from 0 to 99) to num untill that integer lies between 1 and 52 (inclusive) 
      {
        num2 = (int)(100 * Math.random()); //generates a random integer from 0-99 
        c2 = num2 % 13; //determines num's equivilant card
        if (c2 == 0)
        {
          c2 = 13;
        }
      }
      while (num3 < 1 || num3 > 52 || num1 == num3 || num2 == num3) //this loop assigns a different random integer (from 0 to 99) to num untill that integer lies between 1 and 52 (inclusive) 
      {
        num3 = (int)(100 * Math.random()); //generates a random integer from 0-99 
        c3 = num3 % 13; //determines num's equivilant card
        if (c3 == 0)
        {
          c3 = 13;
        }
      }
      while (num4 < 1 || num4 > 52 || num1 == num4 || num2 == num4 || num3 == num4) //this loop assigns a different random integer (from 0 to 99) to num untill that integer lies between 1 and 52 (inclusive) 
      {
        num4 = (int)(100 * Math.random()); //generates a random integer from 0-99 
        c4 = num4 % 13; //determines num's equivilant card
        if (c4 == 0)
        {
          c4 = 13;
        }
      }
      while (num5 < 1 || num5 > 52 || num1 == num5 || num2 == num5 || num3 == num5 || num4 == num5) //this loop assigns a different random integer (from 0 to 99) to num untill that integer lies between 1 and 52 (inclusive) 
      {
        num5 = (int)(100 * Math.random()); //generates a random integer from 0-99 
        c5 = num5 % 13; //determines num's equivilant card
        if (c5 == 0)
        {
          c5 = 13;
        }
      }
      
      //four of a kind
      if ( (c1 == c2) && (c1 == c3) && (c1 == c4) ) //if all but the fifth card are the same
      {
        four++; //increments four
        fourDetected = c1; //records the number of the match
      }
      if ( (c1 == c2) && (c1 == c3) && (c1 == c5) ) //if all but the fourth card are the same
      {
        four++;
        fourDetected = c1;
      }
      if ( (c1 == c2) && (c1 == c4) && (c1 == c5) ) //if all but the third card are the same
      {
        four++;
        fourDetected = c1;
      }
      if ( (c1 == c3) && (c1 == c4) && (c1 == c5) ) //if all but the second card are the same
      {
        four++;
        fourDetected = c1;
      }
      if ( (c2 == c3) && (c2 == c4) && (c2 == c5) ) //if all but the first card are the same
      {
        four++;
        fourDetected = c2;
      }
      
      //three of a kind
      if ( (c1 == c2) && (c1 == c3) && (c1 != fourDetected)) //if cards 1, 2, 3 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c2) && (c1 == c4) && (c1 != fourDetected)) //if cards 1, 2, 4 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c2) && (c1 == c5) && (c1 != fourDetected)) //if cards 1, 2, 5 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c3) && (c1 == c4) && (c1 != fourDetected)) //if cards 1, 3, 4 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c3) && (c1 == c5) && (c1 != fourDetected)) //if cards 1, 3, 5 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c4) && (c1 == c5) && (c1 != fourDetected)) //if cards 1, 4, 5 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c2 == c3) && (c2 == c4) && (c2 != fourDetected)) //if cards 2, 3, 4 are the same
      {
        three++;
        threeDetected = c2;
      }
      if ( (c2 == c3) && (c2 == c5) && (c2 != fourDetected)) //if cards 2, 3, 5 are the same
      {
        three++;
        threeDetected = c2;
      }
      if ( (c2 == c3) && (c2 == c4) && (c2 != fourDetected)) //if cards 2, 4, 5 are the same
      {
        three++;
        threeDetected = c2;
      }
      if ( (c3 == c4) && (c3 == c5) && (c3 != fourDetected)) //if cards 3, 4, 5 are the same
      {
        three++;
        threeDetected = c3;
      }
      
      //pair and two pair
      if ( (c1 == c2) && (c1 != fourDetected) && (c1 != threeDetected)) //if cards 1, 2 are the same
      {
        if (((c3 == c4 || c3 == c5) && c3 != threeDetected) || ((c4 == c5) && (c4 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c1 == c3) && (c1 != fourDetected) && (c1 != threeDetected)) //if cards 1, 3 are the same
      {
        if (((c2 == c4 || c2 == c5) && c2 != threeDetected) || ((c4 == c5) && (c4 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c1 == c4) && (c1 != fourDetected) && (c1 != threeDetected)) //if cards 1, 4 are the same
      {
        if (((c2 == c3 || c2 == c5) && c2 != threeDetected) || ((c3 == c5) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c1 == c5) && (c1 != fourDetected) && (c1 != threeDetected)) //if cards 1, 5 are the same
      {
        if (((c2 == c3 || c2 == c4) && c2 != threeDetected) || ((c3 == c4) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c2 == c3) && (c2 != fourDetected) && (c2 != threeDetected)) //if cards 2, 3 are the same
      {
        if (((c1 == c4 || c1 == c5) && c1 != threeDetected) || ((c4 == c5) && (c4 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c2 == c4) && (c2 != fourDetected) && (c2 != threeDetected)) //if cards 2, 4 are the same
      {
        if (((c1 == c3 || c1 == c5) && c1 != threeDetected) || ((c3 == c5) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c2 == c5) && (c2 != fourDetected) && (c2 != threeDetected)) //if cards 2, 5 are the same
      {
        if (((c1 == c3 || c1 == c4) && c1 != threeDetected) || ((c3 == c5) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c3 == c4) && (c3 != fourDetected) && (c3 != threeDetected)) //if cards 3, 4 are the same
      {
        if (((c1 == c2 || c1 == c5) && c1 != threeDetected) || ((c2 == c5) && (c2 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c3 == c5) && (c3 != fourDetected) && (c3 != threeDetected)) //if cards 3, 5 are the same
      {
        if (((c1 == c2 || c1 == c4) && c1 != threeDetected) || ((c2 == c4) && (c2 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c4 == c5) && (c4 != fourDetected) && (c4 != threeDetected)) //if cards 4, 5 are the same
      {
        if (((c1 == c2 || c1 == c3) && c1 != threeDetected) || ((c2 == c3) && (c2 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      counter++; //increments the "hand" number
    }
    System.out.println("The number of loops: "+hands); //prints the output
    System.out.println("The probability of four of a kind: "+((double)four/(double)hands));
    System.out.println("The probability of three of a kind: "+((double)three/(double)hands));
    System.out.println("The probability of two pair: "+((double)twoPair/(double)hands));
    System.out.println("The probability of one pair: "+((double)pair/(double)hands));
  }
}