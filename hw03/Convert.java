/* Max Bonzulak, 9-18-18, CSE 002 
   This program converts hurricane info into cubic miles of watershed*/
import java.util.Scanner; //imports scanner class
public class Convert //create the class
{
  public static void main (String [] args) //creates the main method
  {
    Scanner myScanner = new Scanner( System.in ); //declares an instance and calls the scanner constructor
    System.out.print("Enter the affected area in acres: "); //prompts user to input area affected in acres
    double acresAffected = myScanner.nextDouble(); //records area affected in acres
    System.out.print("Enter the rainfall in the affected area: "); //prompts user to input rainfall in inches
    double rainfall = myScanner.nextDouble(); //records rainfall in inches
    
    double sqMilesPerAcre = .0015625; //conversion factor for acres to square miles
    double milesPerIn = .0000157828; //conversion factor for inches to miles
    
    double sqMilesAffected = acresAffected * sqMilesPerAcre; //area affected in square miles
    double milesOfRainfall = rainfall * milesPerIn; //rainfall in miles
    
    double cubicMilesOfRain = sqMilesAffected * milesOfRainfall; //calculates cubic miles of rain
    System.out.println(cubicMilesOfRain+" cubic miles"); //prints calculations
  }
}