/* Max Bonzulak, 9-18-18, CSE 002 
   This program converts finds the volume of a square pyramid*/
import java.util.Scanner; //imports scanner class
public class Pyramid //create the class
{
  public static void main (String [] args) //creates the main method
  {
    Scanner myScanner = new Scanner( System.in ); //declares an instance and calls the scanner constructor
    System.out.print("The square side of the pyramid is (input length): "); //prompts user to input side length
    double side = myScanner.nextDouble(); //records side length
    System.out.print("The height of the pyramid is (input height): "); //prompts user to input height
    double height = myScanner.nextDouble(); //records height
    
    double volume = (side * side)*(height/3); //calculates volume
    System.out.println("The volume inside the pyramid is: "+volume); //prints calculations
  }
}