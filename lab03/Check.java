/* Max Bonzulak, 9-13-18, CSE 002 
   This program splits a check*/
import java.util.Scanner; //imports Scanner class
public class Check //this class splits a check
{
  public static void main (String [] args) //This method splits a check
  {
    Scanner myScanner = new Scanner( System.in ); //declares an instance and calls the scanner constructor
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompts user to input cost
    double checkCost = myScanner.nextDouble(); //takes user input for cost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //prompts user to input tip %
    double tipPercent = myScanner.nextDouble(); //takes user input for tip
    tipPercent /= 100; //converts the percentage into decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); // prompts user to input number of people
    int numPeople = myScanner.nextInt(); //takes user input for number of people
    
    double totalCost; //declares cost
    double costPerPerson; //declares cost per person
    int dollars, dimes, pennies; //declares variables for currency
    
    totalCost = checkCost * (1 + tipPercent); //the cost including tip
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    
    dollars=(int)costPerPerson; //amount payable in dollars
    dimes=(int)(costPerPerson * 10) % 10; //remaining anount payable in dimes
    pennies=(int)(costPerPerson * 100) % 10; //remainging amount payable in pennies
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //prints results
  }
}
