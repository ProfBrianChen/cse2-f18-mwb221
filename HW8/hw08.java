import java.util.Scanner;
public class hw08
{ 
  public static void main(String[] args) 
  { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++)
    { 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    printArray(cards); 
    cards = shuffle(cards); 
    printArray(cards); 
    while(again == 1)
    { 
      hand = getHand(cards, index, numCards); 
      printArray(hand);
      index = index - numCards;
      if (index < numCards-1)
      {
        cards = shuffle(cards);
        index = 51;
      }
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
  public static String[] shuffle(String[] list)
  {
    int i = 0;
    int index = 0;
    String prev0 = "";
    String[] shuf = new String[52];
    while (i <= 448)
    {
      index = (int) (Math.random()*51);
      prev0 = list[0];
      list[0] = list[index];
      list[index] = prev0;
      i++;
    }
    return list;
  }
  public static String[] getHand(String[] list, int index, int numCards)
  {
    int i = 0;
    String[] hand = new String[numCards];
    while (i<=numCards-1)
    {
      hand[i] = list[index];
      i++;
      index--;
    }
    return hand;
  }
  public static void printArray(String[] list)
  {
    int i = 0;
    while (i <= list.length-1)
    {
      System.out.print(list[i]+" ");
      i++;
    }
    System.out.println("");
  }
}
